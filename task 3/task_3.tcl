#!/usr/bin/tclsh

proc fbseries {num} {
	set first 0
	set second 1
	if { $num == 0 } {
	puts "\nReturning zero as there is no zeroth value for fibonacci number!\n"
	set second 0
	} else {
		for {set i 1} {$i < $num} {incr i} {
			set temp [expr $second + $first]
			set first $second
			set second $temp
		}	
	}
	return $second
}
puts "\n======================="
puts "Task 3"
puts "=======================\n"

for {set iter 0} {$iter < 11} {incr iter} {
puts "For $iter number value: [fbseries $iter]"
}
puts "\n=======================\n"
