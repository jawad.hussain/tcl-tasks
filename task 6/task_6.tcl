#!/usr/bin/tclsh

proc single_digit { num } {
set sum 0
for {set i 0} {$i < [string length $num] } { incr i } {
	set sum [expr [string index $num $i] + $sum ]  
}
if {[string length $sum ] > 1} {
	set sum [single_digit $sum]
}
return $sum
}
puts "\n======================="
puts "Task 5"
puts "=======================\n"

puts "Enter decimal get single digit sum: "
gets stdin data 

if { [regexp {^[0-9]+$} $data ]  } {
	puts "\nInput: $data"
	puts "\nOutput: [single_digit $data]"
} else {
    puts "\nEntered data is not an Integer"
}

puts "\n=======================\n"
