#!/usr/bin/tclsh

proc rstring {data} {
	set rdata {}
	set size [string length $data]
	for {set i $size} {$i > 0} {incr i -1} {
	append rdata [string index $data [expr $i -1]]
	}
	return $rdata
}

puts "\n======================="
puts "Task 2"
puts "=======================\n"
puts "Enter strng to reverse: "
gets stdin data 
puts "\nOriginal string: $data"
puts "\nReverse string: [rstring $data]"
puts "\n=======================\n"
