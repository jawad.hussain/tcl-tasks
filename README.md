# TCL Tasks

## How to Run
	Clone repo using command:   
	git clone https://gitlab.com/jawad.hussain/tcl-tasks.git

## Task1:  
	Command:  ./task_1.tcl  
	Discription: Write a program that goes through all inputfiles in a directory and returns name of files that contain some specific text.   
	Note: nevigate to correct folder first  
	
## Task2:  
	Command:  ./task_2.tcl   
	Discription: Write a function that takes a string in  and returns reversed version of string. You are not allowed to use built-in reverse functions.   
	Note: nevigate to correct folder first  
	  
## Task3:  
	Command:  ./task_3.tcl   
	Discription: Write a function that takes a number n in input and returns nth number in a fibonacci series.    
	Note: nevigate to correct folder first  
	  
## Task4:  
	Command:  ./task_4.tcl   
	Discription: Write a program that prints the numbers from 1 to 100. But for multiples of three print "On" instead of the number and for the multiples of five print "Off". For numbers which are multiples of both three and five print "OnOff".   
	Note: nevigate to correct folder first  
  	
## Task5:  
	Command:  ./task_5.tcl  
	Discription: Write a function that takes a positive (>= 0) number and returns its binary representation.   
	Note: nevigate to correct folder first  
  	
## Task6:  
	Command:  ./task_6.tcl  
	Discription: Write a function (using recursion) that takes a positive number in input and returns its single digit representation. A number can be converted into a single digit by adding all digits until the number is less than 10.  
	Note: nevigate to correct folder first  
