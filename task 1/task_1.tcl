#!/usr/bin/tclsh
puts "\n======================="
puts "Task 1"
puts "======================="
set file_name [glob -type f *.txt]
set search "test"
puts "\nSearch: $search"

set files [glob -nocomplain -type f *.txt]
if { [llength $files] > 0 } {
    puts "\nFiles:"
    foreach f_name $files {
        set fin [open $f_name]
	while {[gets $fin line] != -1} {
    		if {[regexp $search $line all value]} {
        		puts $f_name
    		}
	}
	close $fin
    }
} else {
    puts "(no files)"
}
puts "\n=======================\n"
