#!/usr/bin/tclsh

proc binary {i} { 
    set result {}
    while {$i>0} {
        set result [expr {$i % 2 }]$result
        set i [expr {$i/2}]
    }
    if {$result == {}} {
    set res 0
    }
    return $result
}
puts "\n======================="
puts "Task 5"
puts "=======================\n"

puts "Enter decimal to get binary: "
gets stdin data 
if { [regexp {^[0-9]+$} $data ]  } {
puts "\nBinary: [binary $data]"
} else {
    puts "\nEntered data is not an Integer"
}
puts "\n=======================\n"
