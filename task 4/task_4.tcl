#!/usr/bin/tclsh
puts "\n======================="
puts "Task 4"
puts "=======================\n"
for {set i 1} {$i < 101} { incr i} {
	if {([expr $i % 3 ] == 0 ) && ([expr $i % 5 ] == 0 )} {
		puts "OnOff"
	} elseif {[expr $i % 3 ] == 0 } {
		puts "On"
	} elseif {[expr $i % 5] == 0 } {
		puts "Off"
	} else {
	puts $i
	}
}
puts "\n=======================\n"
